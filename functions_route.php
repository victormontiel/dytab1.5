<?php
require_once("functions_EM_2.php");
if(isset($_GET["left_menu"])){
    echo createLeftBar(retrieveViewList(), $_GET["Rol"]);
}
if(isset($_GET["logout"])){
    session_destroy();
    header('Location: ./Login.php'); 
}
if(isset($_POST["create_tables"])){
    echo createAndHideTables($_POST["Rol"]);
}
if(isset($_POST["fetch_data"])){
    echo getDataTable($_POST["view"], $_POST["Rol"]);
}
if(isset($_GET["create_tabs_forms"])){
    echo createTabsAndForms($_GET["ViewName"], $_GET["action"], $_GET["id"], $_GET["Rol"]);
}

?>