<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>MySql Proceedit</title>
<style>
#tabla {
    font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
    border-collapse: collapse;
    width: 50%;
	margin: auto;
}

#tabla td, #tabla th {
    border: 1px solid #ddd;
    padding: 8px;
}

#tabla tr:nth-child(even){background-color: #f2f2f2;}

#tabla tr:hover {background-color: #ddd;}

#tabla th {
    padding-top: 12px;
    padding-bottom: 12px;
    text-align: left;
    background-color: #4CAF50;
    color: white;
}
#espacio {
	text-align: center;
	}
</style>
</head>
<body>
<div id="espacio">
<h1>Mantenimiento Arista Menu</h1>

<?php
include("functions_EM.php");
$ViewList = retrieveViewList();
createTable($ViewList);
?>

</div>
</body>
</html>