<?php

include('functions_EM.php');

if(isset($_GET["TableName"]))
    {
        $TableName = $_GET["TableName"];
    }

 $columnNames=getColumnNames_table($TableName);

 $columnNamesString=getColumnNamesString($columnNames);

 function createTabs($TableName, $columnNames) 
 {
 	if($TableName=="Clientes") 
 	{
 		echo "<button class=\"tablinks\" onclick=\"openForm(event, 'Información básica')\">Información básica</button>";
 		echo "<div id=\"" . $TableName . "\"Form\" class=\"tabcontent\">";

 		createInsertForm($columnNames, $TableName);
  				
		echo "</div>";
 	}
 }

?>

<script type="text/javascript">
	function openForm(evt, TableName) {
    // Declare all variables
    var i, tabcontent, tablinks;

    // Get all elements with class="tabcontent" and hide them
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }

    // Get all elements with class="tablinks" and remove the class "active"
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }

    // Show the current tab, and add an "active" class to the button that opened the tab
    //document.getElementById(TableName).style.display = "block";
    //evt.currentTarget.className += " active";
}
</script>



 <html lang="es">

 	<head>
  		<meta charset="UTF-8">
  		<meta name="viewport" content="width=device-width, initial-scale=1">
  		<!--Import Google Icon Font-->
      	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!--Import materialize.css-->
      	<link type="text/css" rel="stylesheet" href="materialize/css/materialize.min.css"  media="screen,projection"/>
	</head>
	<style>
		div.Form {
    	width: 50%;
    	background-color: azure;
		}
	</style>
	<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
	<script type="text/javascript" src="materialize/js/materialize.min.js"></script>
	<body>
		<div class="tab">
			<!--<?php
			createTabs($TableName, $columnNames);
			?>-->
		</div>
		<h2 class=\"header\" align=\"center\">Nuevo registro</h2>
		<div class="Form" style="width:800px; margin:0 auto;">
			<?php
				createInsertForm($columnNames, $TableName);
			?>
		</div>
	</body>
</html>
