<?php

session_start();
include('functions_EM.php');
$timeout=sessionTimeout();
if(!empty($timeout)) {
        redirectToLogInError("TIMEOUT");
    }

if(isset($_SESSION["Rol"]))
    {
       $Rol=$_SESSION["Rol"];
    }
else {
	redirectToLogIn("CREDENTIALS");
	}


if(isset($_GET["ViewName"]))
    {
        $ViewName = $_GET["ViewName"];
    }

//action = 0 --> insert_nothing --- action = 1 --> update_nothing
 $action=0;
 $Id=-1;
 $IdOportunidad="-1";
 $cif="-1";

 if(isset($_GET["Id"]))
    {
        $Id = $_GET["Id"];
        $action=1;
    }

    if(isset($_GET["IdOportunidad"]))
    {
        $IdOportunidad = $_GET["IdOportunidad"];
    }




$tables=getTablesfromView($ViewName);

//------------------------------------------------------------------------------------------



function createTabsAndForms($tables, $action, $id, $Rol) 
{
	if(empty($Rol)) {
		redirectToLogIn();
		return;
	}

	echo "<div class=\"row\">
    			<div class=\"col s12\">
      				<ul class=\"tabs\">";
	for($i=0;$i<sizeof($tables);$i++) 
	{
		echo "<li class=\"tab col s3\"><a href=\"#$tables[$i]\">$tables[$i]</a></li>";
	}
	echo "</ul></div>";

	for($i=0;$i<sizeof($tables);$i++) 
	{
		echo "<div id=\"$tables[$i]\" class=\"col s12\">";

		createForm($tables[$i], $action, $id);

		echo "</div>";

		//FormJQuery($tables[$i]);
	}
	echo "</div>";

	echo "<script type=\"text/javascript\">
			$(document).ready(function(){
    				$('ul.tabs').tabs();
  			});
  			</script>";


}

function FormJQuery($table) {

	$columnNames=getColumnNames_table($table);

	echo "<script type=\"text/javascript\">";
	echo "$(\"#$table\").validate({
        rules: {";
        $var=0;

    for($i=0;$i<sizeof($columnNames);$i++) {
    	$check=check_mandatory($columnNames[$i], $table);
    	if($check==0) continue;
    	else {
    		if($var==0) {
    			echo "$table" . "_$columnNames[$i]: {
    			required: true,
    		}
    		";
    		$var=1;
    		}
    		else {
    			echo ",
    			$table" . "_$columnNames[$i]: {
    			required: true,
    		}";

    		}
    		
    	}

    }
    echo ");</script>";

}

function check_mandatory($field, $table) {
	include('Mysqlconn.php');

	$query="SELECT IS_NULLABLE FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = '$table' AND COLUMN_NAME='$field'";
	$row_mysqli=$conexion->query($query);

	while ($row = mysqli_fetch_array($row_mysqli, MYSQLI_NUM)) 
	{
		$bool=$row[0];	
	}
	if($bool=='YES') return 0;
	if($bool=='NO') return 1;
}

function getTablesfromView($ViewName) 
{
	$tables=array();
	switch ($ViewName) 
 	{
    	case "ClientesView":
    		array_push($tables, "Clientes");
    		array_push($tables, "ClientesConfiguracionesOperativasDet");
    		array_push($tables, "ClientesDatosEconomicosDet");
    		array_push($tables, "ClientesDatosOperativosDet");
        break;

    	case "OportunidadesView":
        	array_push($tables, "Oportunidades");
        	break;

    	case "CampanasView":
        	array_push($tables, "Campanas");
        	break;
        case "AccionesView":
        	array_push($tables, "AccionesSeguimientoOportunidades");
        	break;
		
		case "ContactosView":
			array_push($tables, "PersonasContactoClientes");
			break;

		case "FacturasView":
			array_push($tables, "Facturas");
			array_push($tables, "FacturasEvolucionDet");
			break;

		case "ClientesCampanasView":
			array_push($tables, "ClientesCampanasRel");
			break;

		case "GruposView":
			array_push($tables, "GruposBonita");
			break;

		case "MembresiasView":
			array_push($tables, "MembresiasBonitaRel");
			break;

		case "RolesView":
			array_push($tables, "RolesBonita");
			break;

		case "UsuariosView":
			array_push($tables, "UsuariosBonita");
		
	}

	return $tables;
}

function createForm($table, $action, $id) 
{
	if($action==0) 
	{
		createInsertForm($table);
	}
	if($action==1) 
	{
		createEditForm($table, $id);
	}

}

function createInsertForm($table) 
{

	$columnNames=getColumnNames_table($table);

	$rcolumnNames=filterColumns($columnNames);

	echo "<fieldset>
    			<form class=\"col s12\" method=\"POST\" action=\"./InsertCode_EM.php\" id=\"$table\">
    				
    				<br><br>";
    echo "<div class=\"row\">
  			<div class=\"column\">";

    echo "<input type =\"hidden\" name=\"TableName\" value=\"$table\"/>";
    echo "<input type =\"hidden\" name=\"mysql\" value=\"insert\"/>";
	
	for($i=0;$i<count($rcolumnNames)/2;$i++) 
	{
		$values=CheckPK_table($rcolumnNames[$i], $table);
		if(empty($values)) 
		{
			createSimpleField($rcolumnNames[$i], $table);
		}
		else 
		{
			createSelect($values, $rcolumnNames[$i], $table);
		}

	}

	echo "</div>";
	echo "<div class=\"column\">";


	for($j=$i;$j<count($rcolumnNames);$j++) 
	{
		$values=CheckPK_table($rcolumnNames[$j], $table);
		if(empty($values)) 
		{
			createSimpleField($rcolumnNames[$j], $table);
		}
		else 
		{
			createSelect($values, $rcolumnNames[$j], $table);
		}


	}
	echo "</div>";
	echo "</div>";
	createFormButtons($table, "Insert");
	
	echo "</form>
			</fieldset>";
}

function createFormButtons($table, $action) 
{
	if($action=="Insert") {
		echo "<button class=\"btn waves-effect waves-light pulse\" type=\"submit\" name=\"action\" onclick=\"return checkMandatorySelects(this.form)\">Guardar registro<i class=\"material-icons right\">cloud</i></button>";
		
		//previous button anadir registro
		//onclick=\"return checkMandatorySelects(this.form)\"
		//"<input type=\"submit\" value=\"Añadir registro\" />";
		echo "<button type=\"button\" class=\"btn waves-effect waves-light\" onclick=\"window.location.href='./EFirstPage.php'\"><i class=\"material-icons right\">home</i>Volver a la tabla</button>";
		
		echo "<button type=\"reset\" class=\"btn waves-effect waves-light\"><i class=\"material-icons right\">autorenew</i>Limpiar</button>";
	}

	if($action=="Update") {
		echo "<button class=\"btn waves-effect waves-light\" type=\"submit\" name=\"action\"><i class=\"material-icons right\" onclick=\"return checkMandatorySelects(this.form)\">send</i>Actualizar registro</button>";
		echo "<button class=\"btn waves-effect waves-light\" onclick=\"window.location.href='./EFirstPage.php'\"><i class=\"material-icons right\">home</i>Volver a la tabla</button>";
		echo "<button type=\"reset\" class=\"btn waves-effect waves-light\"><i class=\"material-icons right\">autorenew</i>Valores iniciales</button>";
	}

}

//confirm('Está seguro de que quiere editar este registro?')

function createEditForm($table, $id) 
{

	$id_value = explode("_", $id);

	$data=getDataEdit($table, $id_value[0], $id_value[1]);
	if(empty($data)) {
		echo "No se han registrado datos todavía. Para añadir datos, volver a la tabla y clicar en nuevo registro" . "<br>";
		return;
	}
	$columnNames=getColumnNames_table($table);
	$rcolumnNames=array();
	for($i=0;$i<count($columnNames);$i++) {
		if(strpos($columnNames[$i], 'Hito')!==false) continue;
		else array_push($rcolumnNames, $columnNames[$i]);
	}

	echo "<fieldset>
    			<form class=\"col s12\" method=\"POST\" action=\"./UpdateCode_EM.php\" id=\"$table\">
    				<br><br>";
    echo "<div class=\"row\">
  			<div class=\"column\">";

    echo "<input type =\"hidden\" name=\"TableName\" value=\"$table\"/>";
    echo "<input type =\"hidden\" name=\"Id\" value=\"$id_value[0]" . "_" . "$id_value[1]\"/>";
	
	for($i=0;$i<count($rcolumnNames)/2;$i++) 
	{
		$values=CheckPK_table($rcolumnNames[$i], $table);
		if(empty($values)) 
		{
			createSimpleField_edit($rcolumnNames[$i], $data[0][$i], $table);
		}
		else 
		{
			createSelect_edit($values, $rcolumnNames[$i], $table, $data[0][$i]);
		}

	}

	echo "</div>";
	echo "<div class=\"column\">";


	for($j=$i;$j<count($rcolumnNames);$j++) 
	{
		$values=CheckPK_table($rcolumnNames[$j], $table);
		if(empty($values)) 
		{
			createSimpleField_edit($rcolumnNames[$j], $data[0][$j], $table);
		}
		else 
		{
			createSelect_edit($values, $rcolumnNames[$j], $table, $data[0][$j]);
		}
	}

	echo "</div>";
	echo "</div>";
	createFormButtons($table, "Update");
	
	echo "</form>
			</fieldset>";

}

function createSelect_edit($values, $col_name, $table, $value) 
{
	include ("Mysqlconn.php");
	$query = "SELECT " . $values[1] . " FROM ARista." . $values[0] . " order by " . $values[1];
	$row_mysqli=$conexion->query($query);
	$select_array = array();

	while ($row = mysqli_fetch_array($row_mysqli, MYSQLI_NUM)) 
	{
		array_push($select_array, $row[0]);		
	}
	if(strpos($col_name, 'Id')!==false) $col_name2 = substr($col_name, 2);
	else $col_name2=$col_name;


	//Get Id name

	$query2 = "SELECT ReferencedColumnName FROM ARista.ForeignKeysAux where ComboValue='"  . $values[1] . "' and TableName='$table'";
	$row_mysqli=$conexion->query($query2);


	while ($row = mysqli_fetch_array($row_mysqli, MYSQLI_NUM)) 
	{
		$id_name=$row[0];		
	}


	$query3 = "SELECT " . $values[1] . " FROM ARista." . $values[0] . " where " . $id_name . "='" . $value . "'";


	$row_mysqli=$conexion->query($query3);

	while ($row = mysqli_fetch_array($row_mysqli, MYSQLI_NUM)) 
	{
		if(empty($row[0])) $value_select="";
		else {
			$value_select=$row[0];
		}
				
	}


	//if(empty($value_select)) $value_select="";
	$mand=check_mandatory($col_name, $table);
	if($mand==1) {
		$star='*';
		$required="required=\"\" aria-required=\"true\"";
		$requiredDiv="name=\"requiredSelectDiv\"";
	}
	else {
		$star='';
		$required="";
		$requiredDiv="";
	}

	echo "<div class=\"input-field col s12\"" . $requiredDiv . ">
			<select id=\"$table" . "_$col_name\" style=\"width: 400px\" name=\"" . $values[1] . "\"" . $required . ">
				<option value=\" \" disabled selected>" . $col_name2 . "</option>";
	for($i=0;$i<count($select_array);$i++) 
	{
		if($select_array[$i]==$value_select) {
			echo "<option selected=\"selected\">$select_array[$i]</option>";
		}
		else {
			echo "<option>$select_array[$i]</option>";
		}
		
	}
	echo "</select>";
			echo "<label>" . $col_name2 . $star . "</label>";
	echo "</div>";
	echo  "<br>";

	initialize_select("$table" . "_$col_name");
	

}



function createSimpleField_edit($FieldName, $value, $table) 
{
/*	if (strpos($FieldName, 'Fecha') !== false) 
	{
		echo "<div class=\"row\">
        		<div class=\"input-field col s6\">
          				<input id=\"$FieldName\" type=\"text\" class=\"datepicker\" name=\"$FieldName\" value=\"$value\"/>
          				<label for=\"$FieldName\">$FieldName</label>
        		</div>
      		 </div> 
      		 <br>";
	}

	else 
	{	
      	echo "<div class=\"row\">
        		<div class=\"input-field col s6\">
          				<input id=\"$FieldName\" type=\"text\" class=\"validate\" name=\"$FieldName\" value=\"$value\"/>
          				<label for=\"$FieldName\">$FieldName</label>
        		</div>
      		 </div> 
      		 <br>";
	}*/
	$fieldType=getFieldType($table, $FieldName);
	$type=FieldTypeToType($fieldType);


	$mand=check_mandatory($FieldName, $table);
	if($mand==1) $star='*';
	else $star='';
	if (strpos($FieldName, 'Fecha') !== false) 
	{
		echo "<div class=\"row\">
        		<div class=\"input-field col s6\">";
        		if($mand==1) {
        			echo "<input id=\"$FieldName\" type=\"text\" required=\"\" aria-required=\"true\" oninvalid=\"this.setCustomValidity('Este campo es obligatorio')\" oninput=\"setCustomValidity('')\" class=\"datepicker\" name=\"$FieldName\" value=\"$value\"/>";
        		}
        		else {
        			echo "<input id=\"$FieldName\" type=\"text\" class=\"datepicker\" name=\"$FieldName\" value=\"$value\"/>";
        		}
          				
          			echo "<label for=\"$FieldName\">$FieldName$star</label>     				
        		</div>
      		 </div>";
	}

	else 
	{	
      	echo "<div class=\"row\">
        		<div class=\"input-field col s6\">";
        		if($mand==1) {
        			echo "<input id=\"$FieldName\" type=" . $type . " required=\"\" aria-required=\"true\" oninvalid=\"this.setCustomValidity('Este campo es obligatorio')\" oninput=\"setCustomValidity('')\" class=\"validate\" name=\"$FieldName\" value=\"$value\"/>";
        		}
        		else {
        			echo "<input id=\"$FieldName\" type=" . $type . " class=\"validate\" name=\"$FieldName\" value=\"$value\"/>";
        		}
          				
          		echo "<label for=\"$FieldName\">$FieldName$star</label>
        		</div>
      		 </div> 
      		 ";
	}
	
	
}

function getDataEdit($table, $pk, $pk_value) 
{
	$columnNames=getColumnNames_table($table);

 	$columnNamesString=getColumnNamesString($columnNames);

	$query="SELECT ".($columnNamesString)." FROM $table where $pk='$pk_value'";

	$data=select_multi($query);

	return $data;
}

function displaycard($ViewName, $cif, $idoportunidad) 
{
	switch($ViewName) {
		case "OportunidadesView":
		echo "<div class=\"row\">
        		<div class=\"col s12 m6\">
          			<div class=\"card blue-grey darken-1\">
            			<div class=\"card-content white-text\">
              				<span class=\"card-title\">Información del cliente</span>
              				<p>CIFNIFNIE: $cif.</p>
            			</div>
          			</div>
        		</div>
      		</div>";
      	break;
      	case "AccionesView":
      	$cif=IdOp_to_cif($idoportunidad);
      	echo "<div class=\"row\">
        		<div class=\"col s12 m6\">
          			<div class=\"card blue-grey darken-1\">
            			<div class=\"card-content white-text\">
              				<span class=\"card-title\">Información del cliente</span>
              				<p>CIFNIFNIE: $cif.</p>
              				<p>IdOperacion: $idoportunidad.</p>
            			</div>
          			</div>
        		</div>
      		</div>";
      	break;
      	default:
      	echo "";
	}

}

function createFormHeader($ViewName, $action, $Rol) {
	if(empty($Rol)) return;
	$View = str_replace("View","", $ViewName);
	if($action==0) echo "<header id=\"BillibHeader\"><h2 align=\"center\">$View: Nuevo registro</h2></header>";
	if($action==1) echo "<header id=\"BillibHeader\"><h2 align=\"center\">$View: Editar registro</h2></header>";
}


?>

<html lang="es">

 	<head>
 		<title>BilliB ARista</title>
  		<meta charset="UTF-8">
  		<meta name="viewport" content="width=device-width, initial-scale=1">
  		<link rel="shortcut icon" type="image/png" href="https://billibfinance.com/wp-content/uploads/2017/10/favicom.png"/>
		<!-- GOOGLE FONTS + ICONS -->
    	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900" rel="stylesheet">
      	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!--Import materialize.css-->
      	<link type="text/css" rel="stylesheet" href="materialize/css/materialize.min.css"  media="screen,projection"/>
		<link type="text/css" rel="stylesheet" href="style/Nuevoregistro_Form.css"/>
	</head>
	<style>
		.column {
    		float: left;
    		width: 50%;
    		padding: 10px;
		}

/* Clear floats after the columns */
		.row:after {
    		content: "";
    		display: table;
    		clear: both;
		}
	</style>
	<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
	<script type="text/javascript" src="materialize/js/materialize.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.14.0/jquery.validate.js"></script>

	<script type="text/javascript">
		function submitForm(table) 
		{
			document.getElementById(table).submit();
		}

		function clearForm(Form) {
			alert('hola');
			//document.getElementById(Form).reset();
			var form = document.getElementById(Form);
			form.reset();
			var frm_elements=form.elements;
			for(var i=0;i<frm.elements.length;i++) {
				if(frm.elements[i].type=="text") {
					var field = Document.getElementById(frm.elements.id).value="";
				}
			}
			alert('x.id');
		}

		$(document).ready(function() {
			$('.datepicker').pickadate(
				{ 
				selectMonths: true,
				 selectYears: 15, 
				 today: 'Today',
				 format: 'yyyy-mm-dd', 
				 clear: 'Clear',
				 close: 'Ok',
				 closeOnSelect: false,
				 formatSubmit: 'yyyy-mm-dd'
				});

			});

		function goBack() {
			window.history.back();
		}

		function digitsOnly(obj) {
   obj.value = obj.value.replace(/[^0-9.,]+/g, "");
}

		function checkMandatorySelects(formDiv) {
			var y = document.getElementById(formDiv.id);
			var x = y.querySelectorAll('[name="requiredSelectDiv"]');
			for(var i=0;i<x.length;i++) {
				var labels = x[i].getElementsByTagName("LABEL");
				var selects = x[i].getElementsByTagName("SELECT");
				if(selects[0].value=="") {
					alert("El campo " + labels[0].innerText.replace("*","") + " es obligatorio.");
					return false;
				}
			}
		}

		</script>
	<body>
		<?php
		createFormHeader($ViewName, $action, $Rol);
		?>
			<div class="Form" style="width:1200px; margin:0% auto;">
				<?php
					createTabsAndForms($tables, $action, $Id, $Rol);
				?>
			</div>
		<footer id="BillibFooter">
			<p style="position: relative; left: 39vw;  bottom: 13px; font-weight: 600; font-size: 13px; color: #3a3a3a;">Powered by dyTAB</p>
			<div class="container">
				<center><a href="http://proceedit.blogspot.com.es/" style="color:black; font-size: 13px; font-weight: 600;">Copyright © 2018 Proceedit, all rights reserved.</a>
			</div>
		</footer>
	</body>
</html>

