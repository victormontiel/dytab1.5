<?php
include("prueba.php");
$countOfColumns = 0;
	$countOfColumnsTest = 0;
	$columnsString = "";
	$where="";
	$querycolumns_names="
SELECT (COLUMN_NAME)
FROM information_schema.COLUMNS
WHERE 
    TABLE_SCHEMA = 'ARista' 
AND TABLE_NAME = 'ClientesView' 
AND COLUMN_NAME NOT lIKE 'Checksum%' AND COLUMN_NAME NOT LIKE 'Id%';";
	$columns = $conexion->query($querycolumns_names);
	$columnsTest = $conexion->query($querycolumns_names);
	
	$countOfColumns = sizeof($columnsTest);
	
	while($row = mysqli_fetch_row($columns))
	{
		$columnsString .= $row[0].",";
		$countOfColumnsTest += 1;
	}
	
	//replace last
	$columnsString = substr($columnsString, 0, strlen($columnsString) - 1);
	
	$queryString="SELECT ".($columnsString)." FROM ClientesView";
	$resultado = $conexion->query($queryString);
	//printf($queryString);
	
?>

<html lang="es">
<head>
		
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/bootstrap-theme.css" rel="stylesheet">
	<link href="css/jquery.dataTables.min.css" rel="stylesheet">
	<script src="js/jquery-3.2.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.dataTables.min.js"></script>
	
	
	
	<script>
	$(document).ready(function() {
		$('#mitabla').DataTable({
		"language":{
				"lengthMenu": "Mostrar _MENU_ registros por pagina",
				"info": "Mostrando pagina _PAGE_ de _PAGES_",
				"infoEmpty": "No hay registros disponibles",
				"infoFiltered": "(filtrada de _MAX_ registros)",
				"loadingRecords": "Cargando...",
				"processing":     "Procesando...",
				"search": "Buscar:",
				"zeroRecords":    "No se encontraron registros coincidentes",
				"paginate": {
					"next":       "Siguiente",
					"previous":   "Anterior"
				},	
				
			
		}
		});
	
    $('#mitabla tfoot th').each( function () {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="Buscar '+title+'" />' );
    } );
 
    // DataTable
    var table = $('#mitabla').DataTable();
 
    // Apply the search
    table.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
    } );
} );
	</script>
</head>
<body>
	<div class="container">
		<div class="row">
			<h2 style="text-align:center">Mantenimiento Arista Clientes</h2>
		</div>
		<div class="row">
			<a href="modif.php?var=<?php echo $columnsString;?>" class="btn btn-primary">Nuevo registro</a>
		</div>
		
		<br>
		
		<div class="row table-responsive">
			<table class="display" id="mitabla">
			<thead>
				<tr>
				
				<?php
				while($row = mysqli_fetch_row($columnsTest)) 
				{
				?>
				
				
				<th><?php printf ("%s \n", $row[0]) ?></th>
				<?php } ?>
				<th>Modificar Registro</th>
				<th>Eliminar Registro</th>	
				
				</tr>
			</thead>
				<tfoot>
				<tr>
				
				<?php
				while($row = mysqli_fetch_row($columnsTest)) 
				{
				?>
				
				<th><?php printf ("%s \n", $row[0]) ?></th>
				<?php } ?>
				<th></th>
				<th></th>		
				
				</tr>
			</tfoot>
			<tbody>

			<?php 
			while($row = mysqli_fetch_row($resultado)) 
			{ ?>
			<tr>
			<!-- Here you have to loop through columns length and display it-->
			<?php for ($i = 0; $i < $countOfColumnsTest -1; $i++) {?>
				
				<td><?php printf ("%s \n", $row[$i]) ?></td>
			<?php }?>
			<td></td>
			<td><a href="modificar_test.php?id=<?php echo $row[$i=0]; ?>"><span class="glyphicon glyphicon-pencil"></span></a></td>
			<td><a href="#" data-href="eliminar.php?id=<?php echo $row[$i=0]; ?>" data-toggle="modal" data-target="#confirm-delete"><span class="glyphicon glyphicon-remove"></span></a></td>
				</tr>
				<?php } ?>
				

			</tbody>
			</table>
			
		</div>
		
	
	</div>
		<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title" id="myModalLabel">Eliminar Registro</h4>
					</div>
					
					<div class="modal-body">
						¿Desea eliminar este registro?
					</div>
					
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
						<a class="btn btn-danger btn-ok">Borrar</a>
					</div>
				</div>
			</div>
		</div>
		 <script>
			$('#confirm-delete').on('show.bs.modal', function(e) {
				$(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
				
				$('.debug-url').html('Delete URL: <strong>' + $(this).find('.btn-ok').attr('href') + '</strong>');
			});
		</script>
</body>
</html>